# Práctico 9 - Medición de objetos
# Marcos Darío Aranda
# Legajo: 210141 

import math

import cv2
import numpy as np

ref_point = []
crop = False
punto1 = True
punto2 = False
punto3 = False
punto4 = False
distancia = True
global contador
contador = 0
def shape_selection(event, x, y, flags, param):
    global ref_point, crop,puntos_nuevo, puntox,puntoy,x1,y1,x2,y2,x3,y3,x4,y4, punto1, punto2, punto3, punto4, distancia

    if distancia == False:
        if event == cv2.EVENT_LBUTTONDOWN:
            ref_point = [(x, y)]
            if punto1 == True:
                 (x1,y1) = (x, y)
                 punto1 = False
                 punto2 = True
            elif punto2 == True:
                 (x2, y2) = (x, y)
                 #cv2.putText(image, " 1.25 mtrs", ((x1 + x2) // 2, (y1 + y2) // 2), 2, 0.8,(0, 0, 255), 1, cv2.LINE_AA)
                 #cv2.line(image, (x1 + 2, y1), (x2, y2), (0, 0, 255), 2)
                 punto2 = False
                 punto3 = True
            elif punto3 == True:
                (x3, y3) = (x, y)
                #cv2.putText(image, " 2.57 mtrs", ((x1 + x3) // 2, (y1 + y3) // 2), 2, 0.8,(0, 0, 255), 1, cv2.LINE_AA)
                #cv2.line(image, (x1 + 2, y1), (x3, y3), (0, 0, 255), 2)
                #cv2.imwrite('Media Real.jpeg', image)
                punto3 = False
                punto4 = True
            elif punto4 == True:
                (x4, y4) = (x, y)
                punto4 = False
                punto1 = True
                distancia = True

            xy = "%d,%d" % (x, y)
            print
            xy
            cv2.circle(image, (x, y), 4, (255, 0, 0), thickness=-1)
            #cv2.putText(image, xy, (x, y), cv2.FONT_HERSHEY_PLAIN, 2.0, (255, 255, 255), thickness=5)
            cv2.imshow("Imagen Original", image)

    elif distancia == True:
        if event == cv2.EVENT_LBUTTONDOWN:
            ref_point = [(x, y)]
            if punto1 == True:
                 (x1,y1) = (x, y)
                 punto1 = False
                 punto2 = True
            elif punto2 == True:
                 (x2, y2) = (x, y)
                 punto1 = True
                 distancia_pixeles = math.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2)
                 distancia_cm = (distancia_pixeles * 125) / 480.0
                 distancia_mtrs = distancia_cm / 100.0
                 cv2.putText(image, "{:.2f} mtrs".format(distancia_mtrs), ((x1 + x2) // 2, (y1 + y2) // 2), 2, 0.8,
                             (0, 0, 255), 1, cv2.LINE_AA)
                 cv2.line(image, (x1 + 2, y1), (x2, y2), (0, 0, 255), 2)
                 cv2.imwrite('Calculo de medidas.jpeg', image)

            xy = "%d,%d" % (x, y)
            print
            xy
            cv2.circle(image, (x, y), 4, (255, 0, 0), thickness=-1)
           # cv2.putText(image, xy, (x, y), cv2.FONT_HERSHEY_PLAIN, 1.0, (255, 255, 255), thickness=2)

            #cv2.imshow("Rectificacion", img)



def rectificar(img, puntos_orig, puntos_dst,w,h):
    #(h, w) = img.shape[:2]
    M = cv2.getPerspectiveTransform(puntos_orig, puntos_dst)
    img_out = cv2.warpPerspective(img, M, (w, h), flags=cv2.INTER_LINEAR)
    #img_out = cv2.warpAffine(img, M, (w, h))
    return img_out


#image = cv2.imread('Interior_casa.jpeg')
image = cv2.imread('Rectificacion.jpeg')
image_medida = cv2.imread('Media Real.jpeg')
(h, w) = image.shape[:2]
clone = image.copy()

#image = cv2.resize(image, (500, 720))
image_medida = cv2.resize(image_medida, (500, 720))
cv2.imshow("Medida Real", image_medida)
cv2.namedWindow("Imagen Original")

cv2.setMouseCallback("Imagen Original", shape_selection)



while True:

    cv2.imshow("Imagen Original", image)
    key = cv2.waitKey(1) & 0xFF

    if key == 114:  # Tecla (r) restaurar la imagen original y permitir realizar una nueva selección
        image = clone.copy()
        #image = cv2.resize(image, (500, 720))
        punto1 = True
        punto2 = False
        punto3 = False
        punto4 = False
        distancia = True

    elif key == 104: #Tecla (h) Rectica una imagen
        puntos_dst = np.float32([[0, 0], [480 - 1, 0], [0, 987 - 1],[ 480 - 1, 987 - 1]])
        puntos_nuevo = np.float32([[x1,y1],[x2,y2],[x3,y3],[x4,y4]])
        img = rectificar(image, puntos_nuevo,puntos_dst,480,987)

        cv2.namedWindow("Rectificacion")
        cv2.setMouseCallback("Rectificacion", shape_selection)
        cv2.imshow("Rectificacion", img)
        cv2.imwrite('Rectificacion.jpeg', img)
        cv2.waitKey(0)

    elif key == 113:  # Tecla (q) finaliza
        cv2.destroyAllWindows()
        break


# See PyCharm help at https://www.jetbrains.com/help/pycharm/
