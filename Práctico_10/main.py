import cv2
import numpy as np

from calibrate import *

#Inicializamos los paramtros deldetector de arucos
parametros = cv2.aruco.DetectorParameters_create()

# Load the predefined dictionary
dictionary = cv2.aruco.Dictionary_get(cv2.aruco.DICT_5X5_100)


cap = cv2.VideoCapture(0)
cap.set(3,1280)
cap.set(4,720)
cont = 0

#Calibración
calibracion = calibracion()
matrix, dist = calibracion.calibracion_cam()
print("Matriz de la camara: ",matrix)
print("Coeficiente de Distorsión: ", dist)

while True:
    ret, frame = cap.read()
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    #Detectamos los marcadores en la imagen
    #Camera matrix: Calibración de la imagen
    esquinas, ids, candidatos_malos = cv2.aruco.detectMarkers(gray, dictionary, parameters = parametros,cameraMatrix = matrix, distCoeff= dist)



    try:
        #Si hay marcadores encontrados por el detector
        if np.all(ids != None):

            #Iterar en marcadores
            for i in range(0, len(ids)):

                #Estime la pose de cada marcador y devuelva los valores rvec y tvec
                rvec, tvec, markerPoints = cv2.aruco.estimatePoseSingleMarkers(esquinas [i],0.02, matrix, dist)

                #Eliminamos el error de la matriz de valores numpy
                (rvec - tvec).any()

                #Dibuja un cuadrado alrededor de los marcadores
                cv2.aruco.drawDetectedMarkers(frame, esquinas)

                #dibujamos los ejes
                cv2.aruco.drawAxis(frame,matrix,dist,rvec,tvec,0.01)

                #Coordenadas x del centro del marcador
                c_x = (esquinas [i][0][0][0]+ esquinas [i][0][1][0] + esquinas [i][0][2][0] +esquinas [i][0][3][0])/4

                #Coordenadas y del centro del marcador
                c_y = (esquinas[i][0][0][1] + esquinas[i][0][1][1] + esquinas[i][0][2][1] + esquinas[i][0][3][1]) / 4

                #Mostramos ID
                cv2.putText(frame,"id"+str(ids[i]),(int(c_x),int(c_y)),cv2.FONT_HERSHEY_SIMPLEX,0.5,(50,255,250),2)

                #Extramos las puntos de las esquinas en coordenadas separadas
                c1 = (esquinas[0][0][0][0], esquinas[0][0][0][1])
                c2 = (esquinas[0][0][1][0], esquinas[0][0][1][1])
                c3 = (esquinas[0][0][2][0], esquinas[0][0][2][1])
                c4 = (esquinas[0][0][3][0], esquinas[0][0][3][1])
                v1, v2 = c1[0],c1[1]
                v3, v4 = c2[0],c2[1]
                v5, v6 = c3[0],c3[1]
                v7, v8 = c4[0],c4[1]

                #Dibujamos Cubo
                #Cara Inferior
                cv2.line(frame, (int(v1), int(v2)), (int(v3), int(v4)), (255, 255, 0), 3)
                cv2.line(frame, (int(v5), int(v6)), (int(v7), int(v8)), (255, 255, 0), 3)
                cv2.line(frame, (int(v1), int(v2)), (int(v7), int(v8)), (255, 255, 0), 3)
                cv2.line(frame, (int(v3), int(v4)), (int(v5), int(v6)), (255, 255, 0), 3)

                #Cara Superior
                cv2.line(frame, (int(v1), int(v2-200)), (int(v3), int(v4-200)), (255, 255, 0), 3)
                cv2.line(frame, (int(v5), int(v6-200)), (int(v7), int(v8-200)), (255, 255, 0), 3)
                cv2.line(frame, (int(v1), int(v2-200)), (int(v7), int(v8-200)), (255, 255, 0), 3)
                cv2.line(frame, (int(v3), int(v4-200)), (int(v5), int(v6-200)), (255, 255, 0), 3)

                #Caras laterales
                cv2.line(frame, (int(v1), int(v2 - 200)), (int(v1), int(v2)), (255, 255, 0), 3)
                cv2.line(frame, (int(v3), int(v4 - 200)), (int(v3), int(v4)), (255, 255, 0), 3)
                cv2.line(frame, (int(v5), int(v6 - 200)), (int(v5), int(v6)), (255, 255, 0), 3)
                cv2.line(frame, (int(v7), int(v8 - 200)), (int(v7), int(v8)), (255, 255, 0), 3)

                #Piramide
                #Cara inferior
                cv2.line(frame, (int(v1), int (v2)), (int(v3), int (v4)), (255,0,255),3)
                cv2.line(frame, (int(v5), int(v6)), (int(v7), int(v8)), (255, 0, 255), 3)
                cv2.line(frame, (int(v1), int(v2)), (int(v7), int(v8)), (255, 0, 255), 3)
                cv2.line(frame, (int(v3), int(v4)), (int(v5), int(v6)), (255, 0, 255), 3)

                #Esquinas
                cex1, cey1 = (v1 + v5) //2, (v2 + v6)//2
                cex2, cey2 = (v3 + v7 ) //2, (v4 + v8)//2
                cv2.line(frame, (int(v1), int(v2)), (int(cex1), int(cey1-200)), (255, 0, 255), 3)
                cv2.line(frame, (int(v5), int(v6)), (int(cex1), int(cey1 -200)), (255, 0, 255), 3)
                cv2.line(frame, (int(v3), int(v4)), (int(cex1), int(cey2 -200)), (255, 0, 255), 3)
                cv2.line(frame, (int(v7), int(v8)), (int(cex1), int(cey2 - 200)), (255, 0, 255), 3)

    except:
            if ids is None or len(ids) == 0:
                print ("*************** Marker Detecction Failed ****************")

    cv2.imshow("Realidad Virtual", frame)

    k = cv2.waitKey(1)

    #Almacenamos las fotos para la calibracion
    if k == 97:
        print("Imagen Guardada")
        cv2.imwrite ("cali{}.png".format(cont), frame)
        cont = cont +1

    if k == 27:
        break
cap.release()
cv2.destroyAllWindows()
# See PyCharm help at https://www.jetbrains.com/help/pycharm/
