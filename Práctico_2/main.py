# Práctico 2 - Segmentando una imagen
# Marcos Darío Aranda
# Legajo: 210141

from cv2 import cv2

img = cv2.imread ('hoja.png',0)
# Para resolverlo podemos usar dos for anidados
for row in range (len(img)):
    for col in range(len(img[row])):
     # Agregar código aquí
         dato = img[row][col]
         #print(img[row][col], end=' ')
         if dato < 200:
            img[row][col] = 0
cv2.imwrite('resultado.png',img)
cv2.imshow('resultado.png',img)
cv2.waitKey(0)
cv2.destroyAllWindows()

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
