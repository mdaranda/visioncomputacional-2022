# Práctico 3 - Propiedades de video
# Marcos Darío Aranda
# Legajo: 210141


import cv2


file_name = input("Nombre del archivo: ")
captura = cv2.VideoCapture(file_name)
fps = int(captura.get(cv2.CAP_PROP_FPS))
width = int(captura.get(cv2.CAP_PROP_FRAME_WIDTH))
height = int(captura.get(cv2.CAP_PROP_FRAME_HEIGHT))
print("FPS: {} Width: {} Height: {}".format(fps, width, height))
framesize = (width, height)

salida = cv2.VideoWriter('VideoSalida.avi', cv2.VideoWriter_fourcc(*'XVID'),fps,framesize,0)

delay = int((1000/fps))
while captura.isOpened():
    ret, imagen = captura.read()
    if ret:
        gray = cv2.cvtColor(imagen, cv2.COLOR_RGB2GRAY)
        salida.write(gray)
        cv2.imshow('Halo - Gris', gray)
        if cv2.waitKey(delay) & 0xFF == 113:
            break
    else:
        break

captura.release()
salida.release()
cv2.destroyAllWindows()

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
