# Práctico 1 - Función en python
# Marcos Darío Aranda
# Legajo: 210141 

def adivinar(intentos):
    import random
    numero = random.randint(0, 100)
    for i in range(intentos, 0, -1):
        guess = int(input('Adivine el número entero: '))

        if guess == numero:
            print('Felicitaciones, adivinaste!!!!')
            resultado = intentos - i
            print('Número de intentos es ' + str(resultado+1))
            return 0
        elif guess < numero:
            print('No, el número es mayor')
        else:
            print('No, el número es menor')
    else:
        print('Superaste el número de intentos permitido.')
    print('Intente nuevamente')
    print('El número que no pudo adivinar fue: '+str(numero))

intentos= int(input('Ingrese la cantidad de intentos: '))
print(adivinar(intentos))

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
