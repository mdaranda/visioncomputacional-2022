# Práctico 6 - Transformación de similaridad
# Marcos Darío Aranda
# Legajo: 210141

import cv2
import numpy as np

ref_point = []
crop = False


def shape_selection(event, x, y, flags, param):
    global ref_point, crop

    if event == cv2.EVENT_LBUTTONDOWN:
        ref_point = [(x, y)]


    elif event == cv2.EVENT_LBUTTONUP:

        ref_point.append((x, y))

        cv2.rectangle(image, ref_point[0], ref_point[1], (0, 255, 0), 2)
        cv2.imshow("Imagen Original", image)


def rotar_trasladar(img, angle=0, tx=0, ty=0, s=1):
    M = np.float32([[s * np.cos(angle), s * np.sin(angle), tx],[-s * np.sin(angle), s * np.cos(angle), ty]])
    (h, w) = img.shape[:2]
    img_out = cv2.warpAffine(img, M, (w, h))
    return img_out


angle = int(input('Ingrese ángulo: '))
tx = int(input('Ingrese centro de rotación tx: '))
ty = int(input('Ingrese centro de rotación ty: '))
s = float(input('Ingrese el factor de escala: '))

image = cv2.imread('halo.jpg')
clone = image.copy()
cv2.namedWindow("Imagen Original")
cv2.setMouseCallback("Imagen Original", shape_selection)

while True:

    cv2.imshow("Imagen Original", image)
    key = cv2.waitKey(1) & 0xFF

    if key == 114:  # Tecla (r) restaurar la imagen original y permitir realizar una nueva selección
        image = clone.copy()
    elif key == 101:  # Tecla (e) guarda la porción de la imagen seleccionada transformada
        crop_img = clone[ref_point[0][1]:ref_point[1][1], ref_point[0][0]:
                                                          ref_point[1][0]]
        img_out = rotar_trasladar(crop_img, angle, tx, ty,s)
        cv2.imwrite('Transformacion Euclidiana - Escalada.jpg', img_out)
        cv2.imshow("Transformacion Euclidiana - Escalada", img_out)
        cv2.waitKey(0)

    elif key == 103:  # Tecla (g) guarda la porción de la imagen seleccionada como una nueva imagen
        crop_img = clone[ref_point[0][1]:ref_point[1][1], ref_point[0][0]:
                                                          ref_point[1][0]]
        cv2.imwrite('Imagen_Recortada.jpg', crop_img)
        cv2.imshow("Imagen_Recortada", crop_img)
        cv2.waitKey(0)

    elif key == 113:  # Tecla (q) finaliza
        cv2.destroyAllWindows()
        break

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
