# Práctico 4 - Manipulación de imágenes
# Marcos Darío Aranda
# Legajo: 210141

import cv2

ref_point = []
cropping = False

def shape_selection(event, x, y, flags, param):
    global ref_point, cropping

    if event == cv2.EVENT_LBUTTONDOWN:
        ref_point = [(x, y)]
    elif event == cv2.EVENT_LBUTTONUP:
        ref_point.append((x, y))
        cv2.rectangle(image, ref_point[0], ref_point[1], (0, 255, 0), 2)
        cv2.imshow("Imagen Original", image)


image = cv2.imread('halo.jpg')
clone = image.copy()
cv2.namedWindow("Imagen Original")
cv2.setMouseCallback("Imagen Original", shape_selection)

while True:

    cv2.imshow("Imagen Original", image)
    key = cv2.waitKey(1) & 0xFF

    if key == 114:     #Tecla (r) restaurar la imagen original y permitir realizar una nueva selección
        image = clone.copy()
    elif key == 103:   #Tecla (g) guarda la porción de la imagen seleccionada como una nueva imagen
        crop_img = clone[ref_point[0][1]:ref_point[1][1], ref_point[0][0]:
                                                          ref_point[1][0]]
        cv2.imwrite('Imagen_Recortada.jpg', crop_img)
        cv2.imshow("Imagen_Recortada", crop_img)
        cv2.waitKey(0)

    elif key == 113:   #Tecla (q) finaliza
        cv2.destroyAllWindows()
        break


# See PyCharm help at https://www.jetbrains.com/help/pycharm/
