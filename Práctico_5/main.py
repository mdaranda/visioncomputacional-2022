# Práctico 5 - Manipulación de imágenes
# Marcos Darío Aranda
# Legajo: 210141

import cv2
import numpy as np

ref_point = []

def shape_selection(event, x, y, flags, param):
    global ref_point, crop

    if event == cv2.EVENT_LBUTTONDOWN:
        ref_point = [(x, y)]


    elif event == cv2.EVENT_LBUTTONUP:

        ref_point.append((x, y))
        cv2.imshow("Imagen Original", image)
        cv2.rectangle(image, ref_point[0], ref_point[1], (0, 255, 0), 2)



def rotar(img, angle=0):
    print("Rotación con respecto al centro")
    (h, w) = img.shape[:2]
    M = cv2.getRotationMatrix2D((w / 2, h / 2), angle, 1)
    img_rotar = cv2.warpAffine(img, M, (w, h))
    return img_rotar

def trasladar(img_rotar, tx=0, ty=0):
    (h, w) = img_rotar.shape[:2]
    M = np.float32([[1, 0, tx], [0, 1, ty]])
    img_trasladar = cv2.warpAffine(img_rotar, M, (w, h))
    return img_trasladar

angle = int(input('Ingrese ángulo de rotación: '))
tx = int(input('Ingrese desplazamiento tx: '))
ty = int(input('Ingrese desplazamiento ty: '))

image = cv2.imread('halo.jpg')
clone = image.copy()
cv2.namedWindow("Imagen Original")
cv2.setMouseCallback("Imagen Original", shape_selection)

while True:

    cv2.imshow("Imagen Original", image)
    key = cv2.waitKey(1) & 0xFF

    if key == 114:     #Tecla (r) restaurar la imagen original y permitir realizar una nueva selección
        image = clone.copy()

    elif key == 103:  # Tecla (g) guarda porción de la imagen rotada más trasladada
        crop_img = clone[ref_point[0][1]:ref_point[1][1], ref_point[0][0]:
                                                          ref_point[1][0]]
        cv2.imwrite('Imagen_Recortada.jpg', crop_img)
        cv2.imshow("Imagen_Recortada", crop_img)
        cv2.waitKey(0)
    elif key == 101:   #Tecla (e) guarda porción de la imagen rotada más trasladada
        crop_img = clone[ref_point[0][1]:ref_point[1][1], ref_point[0][0]:
                                                          ref_point[1][0]]
        img_out = rotar(crop_img, angle)
        img_out = trasladar(img_out, tx, ty)
        cv2.imwrite('Transformacion Euclidiana.jpg', img_out)
        cv2.imshow("Transformacion Euclidiana", img_out)
        cv2.waitKey(0)

    elif key == 113:   #Tecla (q) finaliza
        cv2.destroyAllWindows()
        break



# See PyCharm help at https://www.jetbrains.com/help/pycharm/
